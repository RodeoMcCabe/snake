extends Node

onready var game = load("res://game.tscn")

var current_game

func _ready():
	randomize()

func _input(event):
	if event.is_action_pressed("ui_accept"):
		if current_game == null:
			new_game()
		elif current_game.state != current_game.PLAYING:
			new_game()


func new_game():
	if current_game != null:
		remove_child(current_game)
		current_game.queue_free()
	current_game = game.instance()
	add_child(current_game)
	current_game.connect("game_over", self, "_on_game_over")
	$GUI/Play.hide()


func _on_play_pressed():
	new_game()


func _on_game_over():
	$GUI/Play.show()