extends Node

const CELL_SIZE = 8
const GRID_SIZE = 64

const FOOD = preload("res://food.tscn")

enum {
	PLAYING = 0,
	DEAD = 1 }


onready var player = $FG/Player
onready var clock = $GUI/HBox/Clock
onready var label = $GUI/HBox/ScoreVal
onready var timer = $Timer

var state = PLAYING
var score = 0
var start_time

signal game_over

func _ready():
	set_physics_process(true)
	set_process(true)
	start_time = OS.get_ticks_msec()
	spawn_food()


func _physics_process(delta):
	if state == PLAYING:
		player.move(CELL_SIZE)
		if is_out_of_bounds(player.head.position / CELL_SIZE):
			game_over()


func _process(delta):
	if state == PLAYING:
		update_clock()


func is_out_of_bounds(pos):
	if pos.x < 0 or pos.x > GRID_SIZE - 1:
		return true
	elif pos.y < 0 or pos.y > GRID_SIZE - 1:
		return true
	else:
		return false


func update_clock():
	var msecs = OS.get_ticks_msec() - start_time
	var minutes = msecs / 60000
	var seconds = (msecs % 60000) / 1000
	var hundredths = ((msecs % 60000) % 1000) / 10
	
	if hundredths < 10: # leading zero
		hundredths = '0' + str(hundredths)
	else:
		hundredths = str(hundredths)
	
	clock.text = str(minutes) + ":" + str(seconds) + ":" + hundredths


func spawn_food():
	var valid_pos = false # don't spawn food in a cell where there is a snake segment
	var pos = Vector2()
	
	while !valid_pos:
		pos.x = (randi() % 64 * CELL_SIZE)
		pos.y = (randi() % 64 * CELL_SIZE)
		for s in player.segments:
			if s.position == pos:
				valid_pos = false
				break # prevent subsequent iterations from overwriting valid_pos back to true
			else:
				valid_pos = true
		if player.head.position == pos:
			valid_pos = false
	
	var f = FOOD.instance()
	$FG.add_child(f)
	f.position = pos


func game_over():
	state = DEAD
	$GUI/GameOver.show()
	$GUI/HBox.hide()
	emit_signal("game_over")


func increase_score(amount):
	score += amount
	label.text = str(score)


func _on_Player_ate_food():
	increase_score(1)
	spawn_food()


func _on_Player_hit_self():
	game_over()
