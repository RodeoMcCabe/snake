extends Node


enum {
	NORTH,
	EAST,
	SOUTH,
	WEST }

onready var head = $Head

var segments = []
var direction = EAST # direction the snake is moving

signal ate_food
signal hit_self

func _ready():
	set_process_input(true)
	
	segments.append($Sprite2)
	segments.append($Sprite3)
	segments.append($Sprite4)


func move(cell_size):
	var next_pos = head.position # store before updating head
	update_head(cell_size)
	update_segments(next_pos)
	check_hit_self()


func update_head(move_dist):
	match direction:
		NORTH: head.position.y -= move_dist
		EAST: head.position.x += move_dist
		WEST: head.position.x -= move_dist
		SOUTH: head.position.y += move_dist


func update_segments(next_pos):
	# Each segment must move to where the segment ahead of it was.
	for s in segments: 
		var p = s.position
		s.position = next_pos
		next_pos = p


func check_hit_self(): # check if snake ran into itself
	for s in segments:
		if head.position == s.position:
			emit_signal("hit_self")


func eat(): # when head collides with a green block (food)
	emit_signal("ate_food")
	var new_seg = segments[-1].duplicate()
	add_child(new_seg)
	segments.append(new_seg)
	


func _input(event): # prevent moving in opposite of current direction
	if event.is_action("ui_up") and direction != SOUTH:
		direction = NORTH
	elif event.is_action("ui_right") and direction != WEST:
		direction = EAST
	elif event.is_action("ui_down") and direction != NORTH:
		direction = SOUTH
	elif event.is_action("ui_left") and direction != EAST:
		direction = WEST


# callback for when head collides with something on the grid
func _on_area_entered(area):
	if area.is_in_group("food"):
		eat()
		area.eaten()















