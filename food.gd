extends Area2D

onready var tween = $Tween
onready var sprite = $Sprite

func eaten():
	tween.interpolate_property(sprite, # object
							   "scale", # property
							   sprite.scale, # initial value
							   Vector2(2.0, 2.0), # final value
							   0.5, # duration
							   Tween.TRANS_LINEAR, # transition type
							   Tween.EASE_IN_OUT) # ease type
	tween.interpolate_property($Sprite, # object
							   "modulate:a", # property
							   1.0, # initial value
							   0.0, # final value
							   0.5, # duration
							   Tween.TRANS_LINEAR, # transition type
							   Tween.EASE_IN_OUT) # ease type
	tween.start()

func _on_tween_completed( object, key ):
	queue_free()
